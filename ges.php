<?php

function lettreFromValeur ($val) {
  $lettre = "";
  if ($val <= 5) {
    $lettre = "a";
  }
  if ($val > 5 && $val < 11) {
    $lettre = "b";
  }
  if ($val > 10 && $val < 21) {
    $lettre = "c";
  }
  if ($val > 20 && $val < 36) {
    $lettre = "d";
  }
  if ($val > 35 && $val < 56) {
    $lettre = "e";
  }
  if ($val > 55 && $val < 81) {
    $lettre = "f";
  }
  if ($val > 80) {
    $lettre = "g";
  }
  return $lettre;
}

function coordsFromLettre ($val) {
  $coords = array();
  if ($val == "a") {
    $coords = array(530, 105);
  }
  if ($val == "b") {
    $coords = array(530, 175);
  }
  if ($val == "c") {
    $coords = array(530, 245);
  }
  if ($val == "d") {
    $coords = array(530, 315);
  }
  if ($val == "e") {
    $coords = array(530, 385);
  }
  if ($val == "f") {
    $coords = array(530, 450);
  }
  if ($val == "g") {
    $coords = array(530, 520);
  }
  return $coords;
}

$valeur = $_GET['v'];
$font = "/usr/share/fonts/truetype/freefont/FreeSans.ttf";
if (strpos($valeur, ';') !== false) {
  $values = explode(';', $valeur);
  if ($values[0]) {
    $lettre = strtolower($values[0]);
  }
  $value = $values[2];
} else {
  $value = $valeur;
  $lettre = lettreFromValeur($valeur);
}
$coords = coordsFromLettre($lettre);
$im = imagecreatefrompng(dirname(__FILE__).'/images/ges/ges-' . $lettre . '.png');
$text_color = imagecolorallocate($im, 255, 255, 255);
imagettftext($im, 30, 0, $coords[0], $coords[1],  $text_color, $font, $valeur);

header('Content-Type: image/png');
imagepng($im);
imagedestroy($im);
