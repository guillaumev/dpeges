<?php

function lettreFromValeur ($val) {
  $lettre = "";
  if ($val <= 70) {
    $lettre = "a";
  }
  if ($val > 70 && $val <= 110) {
    $lettre = "b";
  }
  if ($val > 110 && $val <= 180) {
    $lettre = "c";
  }
  if ($val > 180 && $val <= 250) {
    $lettre = "d";
  }
  if ($val > 250 && $val <= 330) {
    $lettre = "e";
  }
  if ($val > 330 && $val <= 420) {
    $lettre = "f";
  }
  if ($val > 420) {
    $lettre = "g";
  }
  return $lettre;
}

function coordsFromLettre ($l) {
  $coords = array();
  if ($l == 'a') {
    $coords = array(525, 105);
  }
  if ($l == 'b') {
    $coords = array(525, 175);
  }
  if ($l == 'c') {
    $coords = array(525, 245);
  }
  if ($l == 'd') {
    $coords = array(525, 315);
  }
  if ($l == 'e') {
    $coords = array(525, 385);
  }
  if ($l == 'f') {
    $coords = array(525, 450);
  }
  if ($l == 'g') {
    $coords = array(525, 520);
  }
  return $coords;
}

$valeur = $_GET['v'];
$font = "/usr/share/fonts/truetype/freefont/FreeSans.ttf";
if (strpos($valeur, ';') !== false) {
  $values = explode(';', $valeur);
  if ($values[0]) {
    $lettre = strtolower($values[0]);
  }
  $value = $values[1];
} else {
  $value = $valeur;
  $lettre = lettreFromValeur($valeur);
}
$coords = coordsFromLettre($lettre);
$im = imagecreatefrompng(dirname(__FILE__).'/images/dpe/dpe-' . $lettre . '.png');
$text_color = imagecolorallocate($im, 255, 255, 255);
imagettftext($im, 30, 0, $coords[0], $coords[1],  $text_color, $font, $value);

header('Content-Type: image/png');
imagepng($im);
imagedestroy($im);
